package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"
)

type Data struct {
    Name string 
    Contact string 
    Message string 
}

func main() {

    file, err := os.Create("example.txt")
    if err != nil {
        panic("Ошибка при создании файла")
    }
    defer file.Close()


    http.HandleFunc("/add", func(w http.ResponseWriter, r *http.Request) {

        var data Data
        err := json.NewDecoder(r.Body).Decode(&data)
        if err != nil {
            w.WriteHeader(http.StatusBadRequest)
            return 
        }

        w.WriteHeader(http.StatusOK)

        fmt.Println("name: ", data.Name)
        fmt.Println("contact: ", data.Contact)
        fmt.Println("message: ", data.Message)

        currentTime := time.Now().Format("2006-01-02 15:04:05")

        file.WriteString("Time: " + currentTime + "\n")
        file.WriteString("Name: " + data.Name + "\n")
        file.WriteString("Contact: " + data.Contact + "\n")
        file.WriteString("Message: " + data.Message + "\n")
        file.WriteString("*---*---*---*---*---*---*---*---*---*\n\n")

    })

    http.Handle("/", http.FileServer(http.Dir("./pink_cheesecake")))
    http.ListenAndServe(":3333", nil)
}
