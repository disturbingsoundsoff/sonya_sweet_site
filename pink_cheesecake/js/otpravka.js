document.getElementById("knopka_otpravit").onclick = () => { 
    let namePole = document.getElementById("name_input"); 
    let contactPole = document.getElementById("contact_input"); 
    let messagePole = document.getElementById("message_input"); 
    otpravka(namePole.value, contactPole.value, messagePole.value); 
    namePole.value = ""; 
    contactPole.value = ""; 
    messagePole.value = ""; 
} 
 
async function otpravka(name, contact, message){ 
    let data = { 
        Name: name,  
        Contact: contact, 
        Message: message 
    } 
 
    let otvetServera = await fetch("http://localhost:3333/add", { 
        method: "POST",  
        body: JSON.stringify(data) 
    }) 
 
    if (!otvetServera.ok){ 
        alert("Ошибка: " + otvetServera.status) 
    } 
    else{ 
        alert("Сообщение отправлено") 
    } 
}

